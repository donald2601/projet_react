import logo from './logo.svg';
import './App.css';
import 'react-bootstrap/Button' ; 
import  'bootstrap/dist/css/bootstrap.min.css' ;

function App() {
  return (
    <div className="App">
      <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8">
        <form>
        <div class="mb-3 mt-3">
          <input type="text" class="form-control" id="nom" name="nom" placeholder='nom'></input>
        </div>

        <div class="mb-3 mt-3">
          
          <input type="text" class="form-control" id="prenom" name="prenom" placeholder='prenom'></input>
        </div>

        <div class="mb-3 mt-3">
          <input type="email" class="form-control" id="email" name="email" placeholder='email'></input>
          
        </div>
        
        {/* <div class="mb-3 mt-3">
          <label class="form-check-label"></label>
          
        </div> */}

        <button type="submit" className='btn btn-primary'>Envoyer</button>
      </form>
        </div>
      </div>
    </div>
      </div>
  );
}

export default App;
